package cl.uchile.dcc.landmarksrecognition;

import com.google.zxing.*;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.multi.qrcode.QRCodeMultiReader;
import com.google.zxing.qrcode.QRCodeWriter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static cl.uchile.dcc.landmarksrecognition.ConvexHull.convex_hull;

public class QRUtil {

    private static final Logger logger = LogManager.getLogger(QRUtil.class);

    public static void writeQRCode(String content, String fileName) {
        QRCodeWriter writer = new QRCodeWriter();
        int width = 256, height = 256;
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB); // create an empty image
        int white = 255 << 16 | 255 << 8 | 255;
        int black = 0;
        try {
            BitMatrix bitMatrix = writer.encode(content, BarcodeFormat.QR_CODE, width, height);
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
                    image.setRGB(i, j, bitMatrix.get(i, j) ? black : white); // set pixel one by one
                }
            }

            try {
                ImageIO.write(image, "jpg", new File(fileName + ".jpg")); // save QR image to disk
            } catch (IOException e) {
                logger.error(e.getCause());
                logger.error(e.getMessage());
            }

        } catch (WriterException e) {
            logger.error(e.getCause());
            logger.error(e.getMessage());
        }
    }

    public static String readQRCode(String fileName) {
        BinaryBitmap bitmap = getBinaryBitmap(fileName);
        if (bitmap == null) {
            return null;
        }

        QRCodeMultiReader reader = new QRCodeMultiReader();
        try {
            Result result = reader.decode(bitmap);
            logger.info(result.getResultPoints());
            return result.getText();
        } catch (Exception e) {
            logger.error(e.getCause());
            logger.error(e.getMessage());
            return null;
        }
    }

    private static BinaryBitmap getBinaryBitmap(String fileName) {
        File file = new File(fileName);
        BufferedImage image;
        BinaryBitmap bitmap;
        try {
            image = ImageIO.read(file);
            int[] pixels = image.getRGB(0, 0, image.getWidth(), image.getHeight(), null, 0, image.getWidth());
            RGBLuminanceSource source = new RGBLuminanceSource(image.getWidth(), image.getHeight(), pixels);
            bitmap = new BinaryBitmap(new HybridBinarizer(source));
        } catch (IOException e) {
            logger.error(e.getCause());
            logger.error(e.getMessage());
            return null;
        }
        return bitmap;
    }

    public static LandmarksSummary readMultiQRCode(String fileName) {
        BinaryBitmap bitmap = getBinaryBitmap(fileName);
        if (bitmap == null) {
            return null;
        }
        QRCodeMultiReader reader = new QRCodeMultiReader();
        try {
            Result[] result = reader.decodeMultiple(bitmap);
            List<Result> lr = new ArrayList<>(Arrays.asList(result));
            List<Point> points = lr.stream()
                    .flatMap(o -> Arrays.stream(o.getResultPoints())
                            .map(r -> new Point((int) r.getX(), (int) r.getY())))
                    .collect(Collectors.toList());
            List<Landmark> landmarks = lr.stream()
                    .map(o -> {
                        List<Point> listPoints = Arrays.stream(o.getResultPoints())
                                .map(p -> new Point((int) p.getX(), (int) p.getY()))
                                .collect(Collectors.toList());
                        return new Landmark(listPoints, o.getText());
                    }).collect(Collectors.toList());
            Point[] arrayPoints = new Point[points.size()];
            points.toArray(arrayPoints);
            List<Point> hull = Arrays.asList(convex_hull(arrayPoints));
            return new LandmarksSummary(landmarks,hull);
        } catch (NotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}
