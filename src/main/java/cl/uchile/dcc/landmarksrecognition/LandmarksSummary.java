package cl.uchile.dcc.landmarksrecognition;

import java.util.List;

public class LandmarksSummary {

    private List<Landmark> landmarks;

    private List<Point> convexHull;

    public List<Landmark> getLandmarks() {
        return landmarks;
    }

    public void setLandmarks(List<Landmark> landmarks) {
        this.landmarks = landmarks;
    }

    public List<Point> getConvexHull() {
        return convexHull;
    }

    public void setConvexHull(List<Point> convexHull) {
        this.convexHull = convexHull;
    }

    public LandmarksSummary(List<Landmark> landmarks, List<Point> convexHull) {
        this.landmarks = landmarks;
        this.convexHull = convexHull;
    }

    @Override
    public String toString() {
        return "LandmarksSummary{" +
                "landmarks=" + landmarks +
                ", convexHull=" + convexHull +
                '}';
    }
}
