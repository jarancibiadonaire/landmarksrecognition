package cl.uchile.dcc.landmarksrecognition;

import java.util.List;

public class Landmark {

    private String content;

    private List<Point> positions;

    public Landmark(List<Point> positions, String content) {
        this.positions = positions;
        this.content = content;
    }

    public List<Point> getPositions() {
        return positions;
    }

    public void setPositions(List<Point> positions) {
        this.positions = positions;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "Landmark{" +
                "content='" + content + '\'' +
                ", positions=" + positions +
                '}';
    }
}
