package cl.uchile.dcc.landmarksrecognition;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class Main {

    private static final String TARGET_FILE_NAME = "target.png";

    private static final Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        LandmarksSummary result=QRUtil.readMultiQRCode(TARGET_FILE_NAME);
        logger.info("::::Landmarks::::");
        result.getLandmarks().stream()
                .forEach(l->logger.info(l.toString()));

        logger.info("::::ConvexHull::::");
        result.getConvexHull().stream()
                .forEach(p->logger.info(p.toString()));
    }
}
